import React from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons'
import { faInfoCircle } from '@fortawesome/free-solid-svg-icons'
import MediaModal from '../MediaModal'

import { API_URL } from '../utils/apiUrl'
import { WEB_URL } from '../utils/webUrl'

export default class Board extends React.Component {
	state = {
		video_details: [],
		title: '',
		url: '',
		editMode: false,
    	copied: false,				
       	open: false,		
    	medium: [],
	}

	componentDidMount(){
		axios
			.get(`${API_URL}/video/video_details`+'?api_token='+localStorage.getItem('api_token'))
			.then(({data}) =>{

				this.setState({
					video_details: data.video_details,
					// name: data.post.name,
					// position: data.post.position,
					// file_path: data.post.display_picture
				});
			})
		axios
			.get(API_URL+'/media'+'?api_token='+localStorage.getItem('api_token'))
			.then(({data}) =>{
				this.setState({
					medium: data.media
				});
			})			
	}

	// handleFileChange(e){
	// 	let file = e.target.files;
	// 	this.state.file_path = '';
	// 	this.setState({
	// 		file_path: e.target.files[0]
	// 	})
	// }

	async handleCopy(e){
		this.setState({
			copied: true,
			url: ''
		})
	}	

	handleEditClick(){
		this.setState({
			editMode: !this.state.editMode,
			title: this.state.video_details.title,
			url: this.state.video_details.url,
		});
	}

	onOpenModal = () => {
		this.setState({ open: true });
	};

	onCloseModal = () => {
		this.setState({ open: false, copied: false, });
	};		

	// handleCancelEditClick(){
	// 	this.setState({
	// 		editMode: !this.state.editMode,
	// 	});		
	// }


	handleInputChange(e){
		this.setState({
			[e.target.name] : e.target.value
		})
	}



	handleUpdateVideo(e){
		e.preventDefault();
		let data = new FormData();
		data.append('url', this.state.url);
		data.append('title', this.state.title);
		axios
			.post(API_URL+'/video/update'+'?api_token='+localStorage.getItem('api_token') , data)
			.then(({data})=>{
				window.location.href="/Video"
			});		
	}		

	render() {
		const { open, editMode ,copied ,url, video_details,medium, title} = this.state;
		return (
			<div>
				<MediaModal 
					open={open} 
					onClose={this.onCloseModal}
					copied={copied}
					medium={medium}
					handleCopy={this.handleCopy.bind(this)}
				/>			
				<ol className="breadcrumb">
				  <li className="breadcrumb-item"><Link to='/'>Dashboard</Link></li>
				  <li className="breadcrumb-item active">Promoting Video</li>
				</ol>
				{!editMode ?
					<div>
						<h1>{video_details.title}</h1>
						<small>
							<button className="btn btn-sm btn-primary" onClick={this.handleEditClick.bind(this)}>  Edit</button>
						</small>						
						<hr/>
						<iframe src={video_details.url} data-autoplay="true" width="800" height="470" scrolling="no" frameBorder="0" allowransparency="true" allowFullScreen={true}></iframe>
					</div>
					:
					<div>
						<form onSubmit={this.handleUpdateVideo.bind(this)}>
						  <fieldset>
						    <legend>Update Video</legend>

						    <FontAwesomeIcon icon={faInfoCircle} /> Click <button className="btn btn-sm btn-success" type="button" onClick={this.onOpenModal}>Open media</button> to view media.
						    <p></p>
						    <div className="form-group">
						      <label>Title</label>
						      <input 
						      	name="title"
						      	type="text" 
						      	className="form-control form-control-lg" 
						      	aria-describedby="titleHelp" 
						      	placeholder="Enter Title" 
						      	value={title}
						      	onChange={this.handleInputChange.bind(this)}
						      	required
						      />
						    </div>
						    <div className="form-group">
						      <label>Url</label>
						      <input 
						      	name="url"
						      	type="text" 
						      	className="form-control form-control-lg" 
						      	aria-describedby="titleHelp" 
						      	placeholder="Enter Url" 
						      	value={url}
						      	onChange={this.handleInputChange.bind(this)}
						      	required
						      />
						    </div>						    

						    <div className="form-group">
								<button className="btn btn-success mr-3" type="submit">Save</button>
								<button className="btn btn-danger" onClick={()=>{this.setState({editMode: false})}}>  Cancel</button>
						    </div>	

						  </fieldset>

						</form>
					</div>
					}
			</div>
		);
	}
}
