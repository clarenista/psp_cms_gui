import React from 'react';
import axios from 'axios';

import { API_URL } from '../utils/apiUrl'

import { Link } from 'react-router-dom';

export default class Home extends React.Component {

	state ={
		lectures:[]
	}

	componentDidMount(){
		axios
			.get(API_URL+'/lectures/all'+'?api_token='+localStorage.getItem('api_token'))
			.then(({data}) =>{
				this.setState({
					lectures: data.lectures
				});
			})
	}

	render() {
		const { lectures } = this.state
		return (
			<div>
				<ol className="breadcrumb">
				  <li className="breadcrumb-item"><Link to='/'>Dashboard</Link></li>
				  <li className="breadcrumb-item active">Lectures</li>
				</ol>			
				<Link to='lectures/create' className="btn btn-success">Create new</Link>
				<p></p>
				<div className="row">
					<div className="col-md-4">
						<div className="list-group">
							{lectures && lectures.map(item=>(
								<Link to={`lectures/${item.id}`} className="list-group-item list-group-item-action" key={item.id}>
							    	{item.title}
								</Link>
							))}

						</div>	
					</div>

					<div className="col-md-8">
						<div className="jumbotron">
						  <h1 className="display-3">List of Lectures</h1>
						  <p className="lead">Here are the list of Lectures in the PSP Website</p>
						  <hr className="my-4" />
						  <p>To add new Lecture, click create new. Click any of the lecture listed on the left panel to view the details</p>

						</div>						
					</div>
				</div>			
			</div>
		);
	}
}
