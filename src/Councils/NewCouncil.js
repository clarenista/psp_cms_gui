import React from 'react';
import {Link } from 'react-router-dom';
import axios from 'axios';

import { API_URL } from '../utils/apiUrl'

export default class NewBoard extends React.Component {

	state={
		name:'',
	}

	handleInputChange(e){
		this.setState({
			[e.target.name] : e.target.value
		})
	}

	handleCreateBoard(e){
		const {history} = this.props
		e.preventDefault();
		let data = new FormData();
		data.append('name', this.state.name);
		axios	
			.post(API_URL+'/councils/save'+'?api_token='+localStorage.getItem('api_token') , data,  {
    headers: {
        'Content-Type': 'multipart/form-data'
    }
  }).then(({data})=>{
				history.push('/councils')
			});
	}

	render() {
		const { name } = this.state;
		return (
			<div>
				<ol className="breadcrumb">
				  <li className="breadcrumb-item"><Link to='/'>Dashboard</Link></li>
				  <li className="breadcrumb-item"><Link to='/councils'>Councils</Link></li>
				  <li className="breadcrumb-item active">Create new Council</li>
				</ol>				
				<div className="row justify-content-center">
					<div className="col-md-9">
						<form onSubmit={this.handleCreateBoard.bind(this)} encType="multipart/form-data" method="post" accept-charset="utf-8">
						  <fieldset>
						    <legend>New Council</legend>

						    <p></p>
						    <div className="form-group">
						      <label>Council Name</label>
						      <input 
						      	name="name"
						      	type="text" 
						      	className="form-control form-control-lg" 
						      	aria-describedby="nameHelp" 
						      	placeholder="Enter council name" 
						      	value={name}
						      	onChange={this.handleInputChange.bind(this)}
						      	required
						      />
						    </div>
						    <div className="form-group">
								<button className="btn btn-success" type="submit">Save</button>
						    </div>	

						  </fieldset>

						</form>			
					</div>
				</div>
			</div>
		);
	}
}
