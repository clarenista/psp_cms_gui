import React from 'react';
import axios from 'axios';

import { API_URL } from '../utils/apiUrl'

import { Link } from 'react-router-dom';

export default class Home extends React.Component {

	state ={
		discussions:[]
	}

	componentDidMount(){
		axios
			.get(API_URL+'/discussions/all'+'?api_token='+localStorage.getItem('api_token'))
			.then(({data}) =>{
				this.setState({
					discussions: data.discussions
				});
			})
	}

	render() {
		const { discussions } = this.state
		return (
			<div>
				<ol className="breadcrumb">
				  <li className="breadcrumb-item"><Link to='/'>Dashboard</Link></li>
				  <li className="breadcrumb-item active">Discussions</li>
				</ol>			
				<Link to='discussions/create' className="btn btn-success">Create new</Link>
				<p></p>
				<div className="row">
					<div className="col-md-4">
						<div className="list-group">
							{ discussions && discussions.map(discussion=>(
								<Link to={`discussions/${discussion.id}`} className="list-group-item list-group-item-action" key={discussion.id}>
							    	{discussion.discussion_title}
								</Link>
							))}
						</div>	
					</div>

					<div className="col-md-8">
						<div className="jumbotron">
						  <h1 className="display-3">Discussions Pages</h1>
						  <p className="lead">Here are the list of discussions in the PSP Website</p>
						  <hr className="my-4" />
						  <p>To add new Discussion Page, click create new. Click any of the discussions listed on the left pannel to view the details</p>

						</div>						
					</div>
				</div>			
			</div>
		);
	}
}
