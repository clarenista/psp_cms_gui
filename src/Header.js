import React from 'react';
import { API_URL } from './utils/apiUrl'


export default class Header extends React.Component {
	state = {
		isDropDown:false,
		dropdown: ''
	}

	handleClick(e){
		this.setState({
			dropdown: e.target.id
		})
	}

	render() {
		const { dropdown } = this.state;
		return (
		  	<div>
				<div className="carousel-inners" style={{'backgroundColor': '#71bc55'}}>
			        <div className="container carousel-items active">
			            <img className="d-block w-100" src={API_URL+'assets/images/homepage/bannerlogo4.jpg'} alt="First slide" height="" />
			        </div>
			    </div>  	
				<nav className="navbar navbar-expand-lg navbar-dark bg-primary">
					<div className="container">
					  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
					    <span className="navbar-toggler-icon"></span>
					  </button>

					  <div className="collapse navbar-collapse" id="navbarColor01">
					    <ul className="navbar-nav mr-auto">
					      <li className="nav-item">
					        <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
					      </li>
					      <li className="nav-item dropdown">
					        <a className="nav-link dropdown-toggle" onClick={this.handleClick.bind(this)} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          Dropdown
					        </a>
					        <div className={dropdown === 'navbarDropdown' ? "dropdown-menu show" : "dropdown-menu"} aria-labelledby="navbarDropdown">
					          <a className="dropdown-item" href="#">Action</a>
					          <a className="dropdown-item" href="#">Another action</a>
					          <div className="dropdown-divider"></div>
					          <a className="dropdown-item" href="#">Something else here</a>
					        </div>
					      </li>
					      <li className="nav-item dropdown">
					        <a className="nav-link dropdown-toggle" onClick={this.handleClick.bind(this)} href="#" id="test" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          test
					        </a>
					        <div className={dropdown === 'test' ? "dropdown-menu show" : "dropdown-menu"} aria-labelledby="test">
					          <a className="dropdown-item" href="#">Action</a>
					          <a className="dropdown-item" href="#">Another action</a>
					          <div className="dropdown-divider"></div>
					          <a className="dropdown-item" href="#">Something else here</a>
					        </div>
					      </li>
					      <li className="nav-item">
					        <a className="nav-link" href="#">About</a>
					      </li>
					    </ul>

					  </div>
					</div>
				</nav>
		  		
		  	</div>
		);
	}
}
