import React from 'react';
import axios from 'axios';

import { API_URL } from '../utils/apiUrl'

import { Link } from 'react-router-dom';

export default class Home extends React.Component {

	state ={
		news:[]
	}

	componentDidMount(){
		axios
			.get(API_URL+'/news/all'+'?api_token='+localStorage.getItem('api_token'))
			.then(({data}) =>{
				this.setState({
					news: data.news
				});
			})
	}

	render() {
		const { news } = this.state
		return (
			<div>
				<ol className="breadcrumb">
				  <li className="breadcrumb-item"><Link to='/'>Dashboard</Link></li>
				  <li className="breadcrumb-item active">News</li>
				</ol>			
				<Link to='news/create' className="btn btn-success">Create new</Link>
				<p></p>
				<div className="row">
					<div className="col-md-4">
						<div className="list-group">
							{news.map(item=>(
								<Link to={`news/${item.id}`} className="list-group-item list-group-item-action" key={item.id}>
							    	{item.news_title}
								</Link>
							))}
						</div>	
					</div>

					<div className="col-md-8">
						<div className="jumbotron">
						  <h1 className="display-3">News Pages</h1>
						  <p className="lead">Here are the list of news in the PSP Website</p>
						  <hr className="my-4" />
						  <p>To add new News Page, click create new. Click any of the news listed on the left pannel to view the details</p>

						</div>						
					</div>
				</div>			
			</div>
		);
	}
}
